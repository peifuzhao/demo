$(document).ready(
				function() {
					function ajaxSubmit(dataType, contentType, message) {
						$('.field-error-message').text('');
						$('#request').text(contentType == 'application/json' ? JSON.stringify(JSON.parse(message), null, '\t') : message);
						$.ajax({
									url : '/demo/users/',
									data : message,
									dataType : dataType,
									contentType : contentType,
									type : 'PUT'
								})
						.done(function(data) {
									if(dataType == 'xml')
										$('#response').text((new XMLSerializer()).serializeToString(data));
									else {
										$('#response').text(JSON.stringify(data, null, '\t'));

										$('#all-message').text(
												data.message[0].message);
									}

								})
						.fail(function(data) {
								var response;

								$('#response').text(dataType == 'xml' ? data.responseText :
										JSON.stringify(JSON.parse(data.responseText),null, '\t'));
								response = (dataType == 'xml' ? $(data) : 
											JSON.parse(data.responseText));
								if (response.message[0].type == 'VALIDATION') {
									$('#all-message')
											.text(
													response.message[0].message);
									response.fieldMessage
											.forEach(function(
													fieldMessage) {
												$('#'+ fieldMessage.field
																		.substring(fieldMessage.field
																				.indexOf('.') + 1)
																+ '-message')
														.text(fieldMessage.message);
											});

								}
							});
					}

					$('#put-user-button-json').click(function() {
						ajaxSubmit('json', 'application/json','{"user" :['
								+ JSON.stringify($(
								'#put-user-form')
								.serializeObject())
						+ ']}');

						event.preventDefault();
					});
					$('#put-user-button-xml').click(function() {
						ajaxSubmit('xml', 'application/json', '{"user" :['
								+ JSON.stringify($(
								'#put-user-form')
								.serializeObject())
						+ ']}');

						event.preventDefault();
					});
					
					$('#put-multi-user-button-json').click(function() {
						ajaxSubmit('json', 'application/json', '{"user":[{"userId":"derp","firstName":"asdf","lastName":"asdf","address":"asdf","email":"asdf@azdsf","city":"asdf","state":"va","zipCode":"asfd"},{"userId":"herp","firstName":"asdf","lastName":"asdf","address":"asdf","email":"asdf@asasdf","city":"asdf","state":"va","zipCode":"asfd"}]}');
						event.preventDefault();
					});
					
					$('#put-multi-user-button-xml').click(function() {
						ajaxSubmit('xml', 'text/xml', '<users><user><address>asdf</address><city>asdf</city><email>asdf@azdsf</email><firstName>asdf</firstName><lastName>asdf</lastName><state>va</state><userId>slerp</userId><zipCode>asfd</zipCode></user><user><address>asdf</address><city>asdf</city><email>asdf@azdsf</email><firstName>asdf</firstName><lastName>asdf</lastName><state>va</state><userId>merp</userId><zipCode>asfd</zipCode></user></users>');
						event.preventDefault();
					});
				});