<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib prefix="page" uri="http://www.opensymphony.com/sitemesh/page"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<c:if test="${!loginPage}">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script type="text/javascript">
		if (typeof jQuery == 'undefined') {
			document
					.write(unescape("%3Cscript src='<c:url value="/resources/js/lib/jquery.min.js"/>' type='text/javascript'%3E%3C/script%3E"));
		}
	</script>
	<script src="<c:url value="/resources/js/common.js"/>" async defer></script>
</c:if>
<link rel="stylesheet"
	href="<c:url value="/resources/css/users/common.css"/>">

<decorator:head />
<title>Demo - <decorator:title /></title>
</head>
<body>
	<div id="header">
		<page:applyDecorator name="header" />
	</div>
	<hr>
	<div id="content">
		<decorator:body />
	</div>
	<hr>
	<div id="footer">
		<page:applyDecorator name="footer" />
	</div>
</body>
</html>