<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>POST Form Demo</title>
</head>
<body>
	<form:form id="user-form" action="/demo/users/" modelAttribute="user" method="POST">
		<div>
			<fieldset id="info">
				<legend>Create or Update User</legend>
				<div>
					<div id="field">
						<div class="field">
							<label for="userId">User Id:</label>
						</div>
						<div class="field">
							<label for="firstName">First Name:</label>
						</div>
						<div class="field">
							<label for="lastName">Last Name:</label>
						</div>
						<div class="field">
							<label for="address">Address:</label>
						</div>
						<div class="field">
							<label for="city">City:</label>
						</div>
						<div class="field">
							<label for="state">State:</label>
						</div>
						<div class="field">
							<label for="zipCode">ZIP Code:</label>
						</div>
						<div class="field">
							<label for="email">Email:</label>
						</div>
					</div>
					<div id="data">
						<div class="data">
							<form:input id="userId" type="text" path="userId"/><span
								id="userId-message" class="field-error-message"><form:errors path="userId"/></span>
						</div>
						<div class="data">
							<form:input id="firstName" path="firstName" type="text"/><span
								id="firstName-message" class="field-error-message"><form:errors path="firstName"/></span>
						</div>
						<div class="data">
							<form:input id="lastName" path="lastName" type="text"/><span
								id="lastName-message" class="field-error-message"><form:errors path="lastName"/></span>
						</div>
						<div class="data">
							<form:input id="address" path="address" type="text"/><span
								id="address-message" class="field-error-message"><form:errors path="address"/></span>
						</div>
						<div class="data">
							<form:input id="city" path="city" type="text"/><span
								id="city-message" class="field-error-message"><form:errors path="city"/></span>
						</div>
						<div class="data">
							<form:input id="state" path="state" type="text"/><span
								id="state-message" class="field-error-message"><form:errors path="state"/></span>
						</div>
						<div class="data">

							<form:input id="zipCode" path="zipCode" type="text"/><span
								id="zipCode-message" class="field-error-message"><form:errors path="zipCode"/></span>
						</div>
						<div class="data">
							<form:input id="email" path="email" type="text"/><span
								id="email-message" class="field-error-message"><form:errors path="email"/></span>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
		<div>
			<input type="submit" id="post-user-button"
				value="POST">
		</div>
	</form:form>
</body>
</html>