<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<!DOCTYPE html>
<html>
<head>
<title>View All Users</title>
<script src="<c:url value="/resources/js/users/users.js"/>" async defer></script>
</head>
<body>
	<form id="user-form">
		<div id="info">
			<c:if test="${empty users}">No Users</c:if>
			<div id="field">
				<c:forEach items="${users}" var="user">
					<div>
						<a class="user field"
							href="<c:url value="/users/${user.userId}/"/>">${user.userId}</a>
					</div>
				</c:forEach>
			</div>
			<div id="data">
				<c:forEach items="${users}" var="user">
					<div>
						<input type="button" value="Delete" id="${user.userId}"
							class="delete data">
					</div>
				</c:forEach>
			</div>
			<div class="clear"></div>
			<div class="spaced">
				<input type="button" id="delete-button" class="data"
					value="Delete herp and derp">
			</div>

			<div class="spaced">
				<label for="search">Search: </label><input name="search" type="text"
					id="search-box" value="${search}"> <input type="submit"
					id="search-button" value="Run Search">
			</div>
			<div class="spaced">
				<input type="button" id="delete-fail-button" class="data"
					value="Delete No Value">
			</div>
			<div class="spaced">
				<div id="error-response" class="output"></div>
			</div>

		</div>
	</form>
	<form>
	
		<sec:authorize access="hasRole('CAP_DOIT')">
			<div class="spaced">
				<a href="<c:url value="/users/modify/"/>">Create or Update
					User</a>
			</div>
		</sec:authorize>
		<sec:authorize url="/users/post">
			<div class="spaced">
				<a href="<c:url value="/users/post/"/>">Form POST Demo</a>
			</div>
		</sec:authorize>
		<div class="output">
			<div>
				<input type="button" value="Fetch JSON" id="get-json-button"><input
					type="button" value="Fetch XML" id="get-xml-button">
			</div>
			<div>
				<div>Response:</div>
				<div id="response" class="data-output"></div>
			</div>
		</div>
	</form>
</body>
</html>