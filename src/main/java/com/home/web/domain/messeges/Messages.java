package com.home.web.domain.messeges;

import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Messages {

	public Messages() {
		super();
		message = new ArrayList<Message>();
	}

	public Messages(Collection<Message> message) {
		super();
		this.message = message;
	}

	private Collection<Message> message;

	public Collection<Message> getMessage() {
		return message;
	}

	public void setMessage(Collection<Message> message) {
		this.message = message;
	}
}
