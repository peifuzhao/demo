package com.home.web.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.home.service.UserService;
import com.home.web.domain.Users;

@Controller
@RequestMapping(value = "services")
public class RestServiceController {

	@Resource
	private UserService userService;

	@RequestMapping(value = "users", method = RequestMethod.GET, produces = {
			"application/json", "application/xml", "text/xml" })
	public @ResponseBody
	Users viewAll(@RequestParam(required = false) String search) {
		return new Users(userService.fetchLike(search));
	}
}
