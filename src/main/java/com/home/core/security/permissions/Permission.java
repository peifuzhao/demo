package com.home.core.security.permissions;

import org.springframework.security.core.Authentication;

public interface Permission {

	public boolean isAllowed(Authentication authentication, Object targetObject);
}
