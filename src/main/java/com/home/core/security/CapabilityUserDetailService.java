package com.home.core.security;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * This class delegates to the injected userDetailsService and then loads
 * capabilities from the database tableName
 * 
 * @author stephen.garlick
 * 
 */
public class CapabilityUserDetailService implements UserDetailsService,
		InitializingBean {

	private UserDetailsService delegatingUserDetailService;
	private String tableName = "role_cap_map";
	private String roleCol = "role";
	private String capCol = "cap";
	private NamedParameterJdbcOperations namedParameterJdbcOperations;
	private RowMapper<GrantedAuthority> capRowMapper = new RowMapper<GrantedAuthority>() {
		@Override
		public GrantedAuthority mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			return new SimpleGrantedAuthority(rs.getString(1));
		}
	};

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		UserDetails userDetails = delegatingUserDetailService
				.loadUserByUsername(username);
		return new User(userDetails.getUsername(), userDetails.getPassword(),
				userDetails.isEnabled(), userDetails.isAccountNonExpired(),
				userDetails.isCredentialsNonExpired(),
				userDetails.isAccountNonLocked(),
				loadCapabiltiesAsRoles(userDetails));
	}

	private Collection<GrantedAuthority> loadCapabiltiesAsRoles(
			UserDetails userDetails) {

		Set<GrantedAuthority> roles = new HashSet<GrantedAuthority>(
				userDetails.getAuthorities());
		if (roles.isEmpty())
			return new ArrayList<GrantedAuthority>();

		Set<String> searchRoles = new HashSet<String>();
		for (GrantedAuthority auth : userDetails.getAuthorities()) {
			searchRoles.add(auth.getAuthority());
		}
		Map<String, Collection<String>> params = new HashMap<String, Collection<String>>();
		params.put("roles", searchRoles);
		roles.addAll(namedParameterJdbcOperations.query("select " + capCol
				+ " from " + tableName + " where " + roleCol + " in (:roles)",
				params, capRowMapper));
		;
		return roles;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (delegatingUserDetailService == null)
			throw new BeanInitializationException(this.getClass().getName()
					+ " must inject a delegatingUserDetailService");
		if (namedParameterJdbcOperations == null)
			throw new BeanInitializationException(this.getClass().getName()
					+ " must inject a namedParameterJdbcOperations");

	}

	public UserDetailsService getDelegatingUserDetailService() {
		return delegatingUserDetailService;
	}

	public String getTableName() {
		return tableName;
	}

	public String getRoleCol() {
		return roleCol;
	}

	public String getCapCol() {
		return capCol;
	}

	public NamedParameterJdbcOperations getNamedParameterJdbcOperations() {
		return namedParameterJdbcOperations;
	}

	public RowMapper<GrantedAuthority> getCapRowMapper() {
		return capRowMapper;
	}

	public void setDelegatingUserDetailService(
			UserDetailsService delegatingUserDetailService) {
		this.delegatingUserDetailService = delegatingUserDetailService;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public void setRoleCol(String roleCol) {
		this.roleCol = roleCol;
	}

	public void setCapCol(String capCol) {
		this.capCol = capCol;
	}

	public void setNamedParameterJdbcOperations(
			NamedParameterJdbcOperations namedParameterJdbcOperations) {
		this.namedParameterJdbcOperations = namedParameterJdbcOperations;
	}

	public void setCapRowMapper(RowMapper<GrantedAuthority> capRowMapper) {
		this.capRowMapper = capRowMapper;
	}

}
