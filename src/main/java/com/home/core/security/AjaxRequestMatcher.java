package com.home.core.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.util.RequestMatcher;

public class AjaxRequestMatcher implements RequestMatcher {

	@Override
	public boolean matches(HttpServletRequest request) {
		if ("XMLHttpRequest".equals(request.getHeader("X-Requested-With")))
			return true;
		return false;
	}

}
